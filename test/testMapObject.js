let mapObject = require("../mapObject");
const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

const add_5 = function (key, val) {
  if (typeof val === "number") {
    return [key, val + 5];
  } else {
    return [key, val];
  }
};

let result = mapObject(testObject, add_5);
console.log(result);
