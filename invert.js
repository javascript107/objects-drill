function invert(obj) {
    // Returns a copy of the object where the keys have become the values and the values the keys.
    // Assume that all of the object's values will be unique and string serializable.
    // http://underscorejs.org/#invert
    let inverted_obj={}
    for (let key in obj){
        inverted_obj[obj[key]]=key
    }
    if(inverted_obj.length==0){
        return {}
    }else{
        return inverted_obj
    }
}

module.exports=invert