

function keys(obj) {
  // Retrieve all the names of the object's properties.
  // Return the keys as strings in an array.
  // Based on http://underscorejs.org/#keys
  let keys=[]
  for(let pair in obj){
    keys.push(pair)

  }
  if(keys.length===0){
    return []
  }else{
    return keys
  }
}

module.exports=keys