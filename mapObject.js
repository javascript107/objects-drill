function mapObject(obj, cb) {
  // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
  // http://underscorejs.org/#mapObject
  let objresult = {};
  for (let key in obj) {
    let new_arr = cb(key, obj[key]);
    objresult[new_arr[0]] = new_arr[1];
  }
  if (objresult.length === 0) {
    return [];
  } else {
    return objresult;
  }
}

module.exports = mapObject;
