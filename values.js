function values(obj) {
    // Return all of the values of the object's own properties.
    // Ignore functions
    // http://underscorejs.org/#values
    let values=[]
    for(let pair in obj){
      values.push(obj[pair])
  
    }
    if(values.length===0){
      return []
    }else{
      return values
    }
}

module.exports=values