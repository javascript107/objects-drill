function defaults(obj, defaultProps) {
  // Fill in undefined properties that match properties on the `defaultProps` parameter object.
  // Return `obj`.
  // http://underscorejs.org/#defaults
  let replaceKey;
  for (let item in defaultProps) {
    replaceKey = item;

    for (let item in obj) {
      if (item === replaceKey) {
        obj[item] = defaultProps[item];
        break;
      }
    }
  }
  return obj;
}
module.exports = defaults;
